const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const mongoose = require('mongoose')
const restify = require('express-restify-mongoose')
const app = express()
const router = express.Router()

app.use(bodyParser.json())
app.use(methodOverride())

const port = process.env.PORT || 3001;
const DB_HOST = process.env.DB_HOST;
const DB_PORT = process.env.DB_PORT;
const DB_NAME = process.env.DB_NAME;
const uri = `mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`;

console.log(uri);

mongoose.connect(uri)
  .catch(err => {
    console.log(err)
  })


restify.serve(router, mongoose.model('Video', new mongoose.Schema({
  type: { type: String, required: true },
  category: {
    name: { type: String, required: true }
  },
  name: { type: String, required: true }
}, {
  versionKey: false
})))

restify.serve(router, mongoose.model('User', new mongoose.Schema({
  username: { type: String, required: true },
  password: { type: String, required: true },
  sessionid: { type: String }
}, {
  versionKey: false
})))

restify.serve(router, mongoose.model('Queue', new mongoose.Schema({
  name: { type: String, required: true },
  username: { type: String, required: true }
}, {
  versionKey: false
})))

app.use(router)

app.listen(port, () => {
  console.log('DB API listening on port 3001')
})